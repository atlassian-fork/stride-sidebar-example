//Data structure for sending a dynamic action as a message to open dialog modal
// We're defining a link that has an action that is targeting a dialog that was defined in the app descriptor.
//https://developer.atlassian.com/cloud/stride/apis/document/marks/action/

let instructions = {
  type: 'doc',
  version: 1,
  content: [
    {
      type: 'heading',
      content: [
        {
          type: 'text',
          text: 'Welcome to the Sidebar Example App '
        }
      ],
      attrs: {
        level: 3
      }
    },
    {
      type: 'paragraph',
      content: [
        {
          type: 'text',
          text: 'Find the source code in the '
        },
        {
          type: 'emoji',
          attrs: {
            shortName: ':bitbucket:'
          }
        },
        {
          type: 'text',
          text: 'Bitbucket Repo',
          marks: [
            {
              type: 'link',
              attrs: {
                href:
                  'https://bitbucket.org/atlassian/stride-sidebar-example/src/master/'
              }
            }
          ]
        }
      ]
    },
    {
      type: 'rule'
    },
    {
      type: 'paragraph',
      content: [
        {
          type: 'text',
          text: 'These following examples focus on using:'
        }
      ]
    },
    {
      type: 'bulletList',
      content: [
        {
          type: 'listItem',
          content: [
            {
              type: 'paragraph',
              content: [
                {
                  type: 'text',
                  text: 'Glances',
                  marks: [
                    {
                      type: 'link',
                      attrs: {
                        href:
                          'https://developer.atlassian.com/cloud/stride/apis/modules/chat/glance/',
                        title: 'Glances Docs'
                      }
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          type: 'listItem',
          content: [
            {
              type: 'paragraph',
              content: [
                {
                  type: 'text',
                  text: 'Sidebar',
                  marks: [
                    {
                      type: 'link',
                      attrs: {
                        href:
                          'https://developer.atlassian.com/cloud/stride/apis/modules/chat/sidebar/',
                        title: 'Action Target Docs'
                      }
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    },
    {
      type: 'panel',
      content: [
        {
          type: 'paragraph',
          content: [
            {
              type: 'text',
              text: 'Instructions: ',
              marks: [
                {
                  type: 'strong'
                }
              ]
            },
            {
              type: 'text',
              text: ' If you',
              marks: [
                {
                  type: 'em'
                }
              ]
            },
            {
              type: 'text',
              text: ' "@mention" ',
              marks: [
                {
                  type: 'strong'
                }
              ]
            },
            {
              type: 'text',
              text:
                'the app by its app name in a message, the app will trigger the instructions to display again. The glance will also update showing the count of @mentions',
              marks: [
                {
                  type: 'em'
                }
              ]
            }
          ]
        }
      ],
      attrs: {
        panelType: 'info'
      }
    },
    {
      type: 'paragraph',
      content: [
        {
          type: 'text',
          text: 'Click the glance for further instructions',
          marks: [
            {
              type: 'strong'
            }
          ]
        }
      ]
    }
  ]
}

module.exports = {
  instructions
}
