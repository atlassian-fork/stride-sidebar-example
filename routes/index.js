const CLIENT_ID = process.env.CLIENT_ID
const CLIENT_SECRET = process.env.CLIENT_SECRET
const API_BASE_URL = 'https://api.atlassian.com'

let express = require('express')
let router = express.Router()
const fs = require('fs')
const path = require('path')
const jwt = require('../lib/jwt.js')
const debug = require('debug')('stride:index-route')
const sendToStride = require('../lib/send-message.js').factory({
  apiBaseUrl: API_BASE_URL,
  clientId: CLIENT_ID,
  clientSecret: CLIENT_SECRET
})

let mentionCount = 1

/* GET home page */
router.get('/', function(req, res) {
  res.redirect('/descriptor')
})

//
// Send descriptor
// -----------------------------------------------------------------------------
router.get('/descriptor', (req, res) => {
  let descriptor = JSON.parse(
    fs.readFileSync(path.join(__dirname, '../app-descriptor.json')).toString()
  )
  descriptor.baseUrl = 'https://' + req.headers.host
  res.contentType = 'application/json'
  res.send(descriptor)
  res.end()
})

//
// Install life cycle webhook
// -----------------------------------------------------------------------------
router.post('/installed', jwt.verifyRequest, async (req, res) => {
  // context.cloudId        - the site id that contains the conversation that the app was installed in.
  // context.userId         - the user id that installed the app.
  // context.conversationId - the conversation id that the app was installed in.
  let context = {
    cloudId: req.body.cloudId,
    userId: req.body.userId,
    conversationId: req.body.resourceId
  }

  let {instructions} = await require('../messages/instructions')

  await sendToStride.sendMessage(
    context.cloudId,
    context.conversationId,
    instructions,
    function() {
      //Stop Webhook from sending 3 times by returning 200;
      res.status(200)
    }
  )
  res.status(200).end()
})

router.post('/uninstalled', jwt.verifyRequest, (req, res) => {
  let context = {
    cloudId: req.body.cloudId,
    userId: req.body.userId,
    conversationId: req.body.resourceId
  }

  //TODO: Use context to remove the data from your data store
  debug('Uninstalled payload:', context)

  res.status(200).end()
})

// // If the bot is mentioned it will send an instructions on how to use the app
// // action button
// // -----------------------------------------------------------------------------
router.post('/bot-mention', jwt.verifyRequest, (req, res) => {
  let {instructions} = require('../messages/instructions')
  let {
    body: {cloudId, conversation, userId}
  } = req

  var glanceKey = 'app-glance'
  let glaceUpdate = {
    label: `# of Mentions: ${mentionCount++} `,
    metadata: {}
  }

  //Update Glance Count
  sendToStride.sendGlanceUpdate(
    cloudId,
    conversation.id,
    userId,
    glaceUpdate,
    glanceKey,
    function() {
      //Send Instructions
      sendToStride.sendMessage(
        cloudId,
        conversation.id,
        instructions,
        function() {
          //Stop Webhook from sending 3 times by returning 200;
          res.send('200')
        }
      )
    }
  )
})

// //
// // Sidebar
// // Render Sidebar content
// // -----------------------------------------------------------------------------
router.get('/sidebar', jwt.verifyRequest, (req, res) => {
  let context = {
    cloudId: req.body.cloudId,
    userId: req.body.userId,
    conversationId: req.body.resourceId
  }
  debug(context)
  res.render('sidebar', {
    title: 'Sidebar',
    content: 'This is a sidebar example',
    linkText: 'link to documentation',
    link:
      'https://developer.atlassian.com/cloud/stride/learning/glances-sidebars/#sidebars'
  })
})
router.get('/glance/state', (req, res) => {
  debug('body', req.body)
  res.send({
    label: {
      value: 'Sidebar'
    }
  })
})

module.exports = router
